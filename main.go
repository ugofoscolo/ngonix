package main

import (
	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

func homeLink(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome home!")
}

func ping (w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Pong!")
}

func logger(next http.Handler) http.Handler {
    connection++
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        // Do stuff here
        log.Println(r.RequestURI)
        // Call the next handler, which can be another middleware in the chain, or the final handler.
        next.ServeHTTP(w, r)
    })
}

func health(w http.ResponseWriter, r*http.Request) {
	fmt.Fprintf(w, "{\"connections\": %v}", connection)
}

func main() {
	var connection = 0
	r := mux.NewRouter().StrictSlash(true)
	r.HandleFunc("/", homeLink)
	r.HandleFunc("/ping", ping).Methods("GET")
	r.HandleFunc("/health", health).Methods("GET")
	r.Use(logger)
	log.Fatal(http.ListenAndServe(":8080", r))
}
